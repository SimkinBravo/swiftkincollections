//
//  CollectionView.swift
//  teleSUR
//
//  Created by Patricio Bravo Cisneros on 12/02/19.
//  Copyright © 2019 teleSUR. All rights reserved.
//

import UIKit
import Cartography

class CollectionContainer: UIView {

//    weak var collectionView: UICollectionView

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Coder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

}
