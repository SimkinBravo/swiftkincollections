//
//  CellFactory.swift
//  teleSUR
//
//  Created by Patricio Bravo Cisneros on 12/10/18.
//  Copyright © 2018 teleSUR. All rights reserved.
//

import UIKit
//import Alamofire
//import AlamofireImage

protocol InteractiveFactoryProtocol {

    var didHeaderEventHandlers: [((CellAction) -> Void)] { get set }
    var didCellEventHandlers: [((CellAction) -> Void)] { get set }

    func sendCellAction(_ action: CellAction)
}

protocol CellFactoryProtocol {

    var gridLayout: GridLayoutProtocol { get set }

    func getReusableCell(for collectionView: UICollectionView, at indexPath: IndexPath, in section: SectionData) -> UICollectionViewCell
    func getReusableSuplementaryView(ofKind kind: String, for collectionView: UICollectionView, at indexPath: IndexPath, section: SectionData) -> UICollectionReusableView
}

enum ReusableViewType {

    case header
    case cell
}

class SubmenuCellFactory: CellFactory {

    var submenuIsHidden = true

    override public func sendCellAction(_ action: CellAction) {
        var newAction = action
        if !submenuIsHidden && action.id == "click" {
            newAction.id = "submenu-click"
        }
        super.sendCellAction(newAction)
    }

}

class CellFactory: CellFactoryProtocol, ReusableViewActionsHandlerProtocol {

    var headerActionsHandlers: [((CellAction) -> Void)] = []
    var cellActionsHandlers: [((CellAction) -> Void)] = []

    var gridLayout: GridLayoutProtocol = GridLayout(columns: 2)

    // Mark: UICollectionView-ViewModel Methods
    public func getReusableCell(for collectionView: UICollectionView, at indexPath: IndexPath, in section: SectionData) -> UICollectionViewCell {

        let reuseId = getReuseId(for: collectionView, withSectionData: section, at: indexPath)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath)
        setupInteractiveReusableView(cell, at: indexPath, type: .cell)
        return cell
    }

    public func getReusableSuplementaryView(ofKind kind: String, for collectionView: UICollectionView, at indexPath: IndexPath, section: SectionData) -> UICollectionReusableView {

        let reuseId = getHeaderReuseId(section: section)
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: reuseId, for: indexPath)
        setupInteractiveReusableView(view, at: indexPath, type: .header)
        setupReusableView(view, dataSection: section)
        return view
    }

    public func sendCellAction(_ action: CellAction) {

        cellActionsHandlers.forEach { handler in
            handler(action)
        }
    }

    public func sendHeaderAction(_ action: CellAction) {

        headerActionsHandlers.forEach { handler in
            handler(action)
        }
    }

    private func getReuseId(for collectionView: UICollectionView, withSectionData section: SectionData, at indexPath: IndexPath) -> String {
        
        let data = section.items[indexPath.row]
        if data.model.reuseId != nil {
            return data.model.reuseId
        }
        return "GenericCVC"
    }

    private func getHeaderReuseId(section: SectionData) -> String {

        if section.headerModel.model.reuseId != nil {
            return section.headerModel.model.reuseId
        }
        return "GenericRV"
    }

    private func setupInteractiveReusableView(_ view: UIView, at indexPath: IndexPath, type: ReusableViewType) {

        guard var interactiveReusableView = view as? InteractiveReusableViewProtocol else { return }
        interactiveReusableView.didEventHandler = { [weak self] action in
            guard let self = self else { return }
            switch type {
            case .cell:
                self.sendCellAction(CellAction(indexPath: indexPath, id: action.id, value: action.value))

            case .header:
                self.sendHeaderAction(CellAction(indexPath: indexPath, id: action.id, value: action.value))
            }
        }
    }

    private func setupReusableView(_ view: UIView, dataSection: SectionData) {

        guard let reusableView = view as? ReusableViewProtocol else { return }
        reusableView.willDisplay(with: dataSection.headerModel)
    }

}
