//
//  SubmenuDataSource.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 22/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit

class SubmenuDataSource: InfinityDataSource {

    var submenuItems: [ReusableViewDataProtocol] = []
    var lastGridLayout: GridLayoutProtocol!
    var menuGridLayout: GridLayout = GridLayout(columns: 1, heightProportion: 0.15)

    var isSubmenuHidden: Bool {
        get {
            return _isSubMHidden
        }
        set(newValue) {
            _isSubMHidden = newValue
            setIsSubmenuHidden()
        }
    }
    private var _isSubMHidden = true

    private func setIsSubmenuHidden() {
        guard let factory = cellFactory as? SubmenuCellFactory else { return }
        factory.submenuIsHidden = isSubmenuHidden
        if isSubmenuHidden {
            cellFactory.gridLayout = lastGridLayout
        } else {
            lastGridLayout = cellFactory.gridLayout
            cellFactory.gridLayout = menuGridLayout
        }
    }

    public func showSubmenu() {
        setSection(SectionData(items: submenuItems, headerModel: ReusableViewData(model: ReusableViewModel(reuseId: HeaderReusableView.reuseId, heightProportion: 0.15))))
        isSubmenuHidden = false
    }

    public func getData(forSubmenuAt index: IndexPath) -> ReusableViewDataProtocol {
        return submenuItems[index.row]
    }

    override func getCellFactory() -> CellFactoryProtocol {
        return SubmenuCellFactory()
    }

    override func headerEventHandler(action: CellAction) {
        switch action.id {
        case "show-submenu":
            showSubmenu()
        default:
            print(action)
        }
    }

    override func cellEventHandler(action: CellAction) {
        super.cellEventHandler(action: action)
        switch action.id {
        case "submenu-click":
            isSubmenuHidden = true
        default:
            print("")
        }
    }

}
