//
//  InfinityDataSource.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 22/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit

class InfinityDataSource: DataSource {

    private let aiCellAdapter = [ReusableViewData(model: ReusableViewModel(reuseId: ActivityIndicatorCollectionViewCell.reuseId, heightProportion: 0.2, columns: 1))]

    var isLoadingNextPage = false
    var currentPage = 1
    var infinityScrollEnabled: Bool = true

    override init() {
        super.init()
        initCellFactoryHandlers()
    }

    override func updateSection(at sectionIndex: Int, with items: [ReusableViewDataProtocol], messageEnabled: Bool! = true) {
        
        if isLoadingNextPage {
            sections[sectionIndex].items.removeLast()
            sections[sectionIndex].items.append(contentsOf: items)
            isLoadingNextPage = false
        } else {
            super.updateSection(at: sectionIndex, with: items)
        }
        if infinityScrollEnabled {
            sections[sectionIndex].items.append(contentsOf: aiCellAdapter)
        }
        onDataChange?(BasicAction(id: "section", value: sectionIndex as AnyObject))
    }

    override func clearSections() {
        super.clearSections()
        currentPage = 1
    }

    public func loadNextPage() {
        if isLoadingNextPage {
            return
        }
        isLoadingNextPage = true
        currentPage += 1
        onDataChange?(BasicAction(id: "next-page"))
    }

    internal func cellEventHandler(action: CellAction) {
        switch action.id {
        case "will-display":
            loadNextPage()
        default:
            print(action)
        }
    }

    internal func headerEventHandler(action: CellAction) {   }

    private func initCellFactoryHandlers() {
        guard var cellFactory = cellFactory as? ReusableViewActionsHandlerProtocol else { return }
        cellFactory.cellActionsHandlers.append { [weak self] action in
            guard let self = self else { return }
            self.cellEventHandler(action: action)
        }
        cellFactory.headerActionsHandlers.append { [weak self] action in
            guard let self = self else { return }
            self.headerEventHandler(action: action)
        }

    }

}
