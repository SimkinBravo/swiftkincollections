//
//  CellAction.swift
//  teleSUR
//
//  Created by Patricio Bravo Cisneros on 12/02/19.
//  Copyright © 2019 teleSUR. All rights reserved.
//

import UIKit

struct CellAction {
    
    let indexPath: IndexPath
    var id: String
    let value: AnyObject!
    
    init(indexPath: IndexPath, id: String! = "", value: AnyObject! = nil) {
        self.indexPath = indexPath
        self.id = id
        self.value = value
    }
}

protocol ReusableViewActionsHandlerProtocol {
    
    var headerActionsHandlers: [((CellAction) -> Void)] { get set }
    var cellActionsHandlers: [((CellAction) -> Void)] { get set }
    
    func sendCellAction(_ action: CellAction)
}
