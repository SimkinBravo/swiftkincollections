//
//  DataProvider.swift
//  teleSUR
//
//  Created by Patricio Bravo Cisneros on 21/10/18.
//  Copyright © 2018 teleSUR. All rights reserved.
//

import UIKit

protocol DataSourceProtocol: UICollectionViewDataSource {

    var cellFactory: CellFactoryProtocol! { get set }
    var defaultSectionData: SectionData! { get set }

    var numberOfSections: Int { get }

    var onDataChange: ((BasicAction) -> Void)! { get set }

    func getData(for indexPath: IndexPath) -> ReusableViewDataProtocol
    func removeData(at indexPath: IndexPath)
    func updateData(_ data: ReusableViewDataProtocol, at indexPath: IndexPath)
    func getNumberOfItems(for section: Int) -> Int

    func getSection(at index: Int) -> SectionData!
    func setSections(_ source: [SectionData])
    func setSection(_ source: SectionData)
    func addSection(_ source: SectionData)
    func addSection(_ source: SectionData, at section: Int)
    func updateSection(at sectionIndex:Int, with items: [ReusableViewDataProtocol], messageEnabled: Bool!)
    func clearSections()
}

class DataSource: NSObject, DataSourceProtocol {

    public var cellFactory: CellFactoryProtocol!
    public var onDataChange: ((BasicAction) -> Void)!
    public var defaultSectionData: SectionData!

    internal var sections: [SectionData] = []

    override init() {
        super.init()
        cellFactory = getCellFactory()
    }

    internal func getCellFactory() -> CellFactoryProtocol {
        return CellFactory()
    }

    var numberOfSections: Int {
        return sections.count
    }

    func getNumberOfItems(for section: Int) -> Int {
        return sections[section].items.count
    }

    func getData(for indexPath: IndexPath) -> ReusableViewDataProtocol {
        return sections[indexPath.section].items[indexPath.row]
    }

    func clearSections() {
        sections = []
        onDataChange?(BasicAction(id: "clear"))
    }

    func getSection(at index: Int) -> SectionData! {
        return sections[index]
    }

    func removeData(at indexPath: IndexPath) {
        sections[indexPath.section].items.remove(at: indexPath.row)
        onDataChange?(BasicAction(id: "section", value: indexPath as AnyObject))
    }

    func updateData(_ data: ReusableViewDataProtocol, at indexPath: IndexPath) {
        var items = sections[indexPath.section].items
        items[indexPath.row] = data
        sections[indexPath.section].items = items
        onDataChange?(BasicAction(id: "update-item", value: indexPath as AnyObject))
    }

    func updateSection(at sectionIndex:Int, with items: [ReusableViewDataProtocol], messageEnabled: Bool! = true) {
        if sections.count <= sectionIndex {
            guard var sectionData = defaultSectionData else { return }
            sectionData.items = items
            addSection(sectionData)
        } else {
            sections[sectionIndex].items.append(contentsOf: items)
        }
        if messageEnabled {
            onDataChange?(BasicAction(id: "section", value: sectionIndex as AnyObject))
        }
    }

    //Replace sections with new array
    func setSections(_ source: [SectionData]) {
        sections = source
        onDataChange?(BasicAction(id: "section"))
    }

    //Replace sections with new sectionData
    func setSection(_ source: SectionData) {
        sections = [source]
        onDataChange?(BasicAction(id: "section"))
    }

    func addSection(_ source: SectionData) {
        sections.append(source)
        onDataChange?(BasicAction(id: "section"))
    }

    func addSection(_ source: SectionData, at section:Int) {
        sections.insert(source, at: section)
        onDataChange?(BasicAction(id: "section"))
    }

}

extension DataSource: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return getNumberOfItems(for: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        return cellFactory.getReusableCell(for: collectionView, at: indexPath, in: sections[indexPath.section])
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        guard kind == UICollectionView.elementKindSectionHeader else {

            return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: EmptyFooterView.reuseId, for: indexPath)
        }

        return cellFactory.getReusableSuplementaryView(ofKind: kind, for: collectionView, at: indexPath, section: sections[indexPath.section])
    }

}

class EmptyFooterView: UICollectionReusableView {
    
    static public let reuseId = "EmptyFooterRV"
    
    static func register(at collectionView: UICollectionView) {
        collectionView.register(EmptyFooterView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: EmptyFooterView.reuseId)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
