//
//  ActivityIndicatorCollectionViewCell.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 21/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import Cartography

class ActivityIndicatorCollectionViewCell: UICollectionViewCell, InteractiveReusableViewProtocol {

    static public let reuseId = "ActivityIndicatorCVC"

//    var activityIndicator: NVActivityIndicatorView!
    var didEventHandler: ((BasicAction) -> Void)!

    static func register(at collectionView: UICollectionView) {
        collectionView.register(ActivityIndicatorCollectionViewCell.self, forCellWithReuseIdentifier: ActivityIndicatorCollectionViewCell.reuseId)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
//        activityIndicator.stopAnimating()
    }

    private func initElements() {

//        activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40), type: .ballBeat, color: .blue)
//        addSubview(activityIndicator)
//
//        constrain(activityIndicator) { ai in
//            ai.centerX == ai.superview!.centerX
//            ai.centerY == ai.superview!.centerY
//        }
    }
}

extension ActivityIndicatorCollectionViewCell: ReusableViewProtocol {

    func willDisplay(with data: ReusableViewDataProtocol) {
//        activityIndicator.startAnimating()
        DispatchQueue.main.async {
            self.didEventHandler?(BasicAction(id: "will-display"))
        }
    }

}
