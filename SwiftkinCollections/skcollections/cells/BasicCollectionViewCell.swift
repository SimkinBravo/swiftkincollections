//
//  BasicCollectionViewCell.swift
//  teleSUR Videos
//
//  Created by Patricio Bravo Cisneros on 15/01/19.
//  Copyright © 2019 teleSUR. All rights reserved.
//

import UIKit

class BasicCollectionViewCell: UICollectionViewCell {

    internal func getLabel(text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black) -> UILabel {
        let label = UILabel()
        configure(label, text: text, font: font, textColor: textColor)
        return label
    }

    internal func configure(_ label: UILabel, text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black) {
        label.text = text
        label.font = font
        label.textColor = textColor
        addSubview(label)
    }

}

//extension TutorialPagerViewCell {
//
//    internal func getLabel(text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black) -> UILabel {
//        let label = UILabel()
//        configure(label, text: text, font: font, textColor: textColor)
//        return label
//    }
//
//    internal func configure(_ label: UILabel, text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black) {
//        label.text = text
//        label.font = font
//        label.textColor = textColor
//        addSubview(label)
//    }
//
//}

class BasicCollectionReusableView: UICollectionReusableView {

    internal func getLabel(text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black) -> UILabel {
        let label = UILabel()
        configure(label, text: text, font: font, textColor: textColor)
        return label
    }
    
    internal func configure(_ label: UILabel, text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black) {
        label.text = text
        label.font = font
        label.textColor = textColor
        addSubview(label)
    }
}

//extension BasicCollectionReusableView {
//
//    internal func getFormButton(text: String, style: ButtonStyle! = .rouge) -> FormButton {
//
//        let button = FormButton(type: .custom)
//        button.setTitle(text, for: .normal)
//        button.titleLabel?.font = .buttonTitle
//        button.layer.cornerRadius = 4
//
//        setupButtonStyle(button, style: style)
//        addSubview(button)
//        return button
//    }
//
//    internal func setupButtonStyle(_ button: UIButton, style: ButtonStyle! = .rouge) {
//
//        button.backgroundColor = style.getBackgroundColor()
//        button.setTitleColor(style.getTitleColor(), for: .normal)
//        button.dropShadow(radius: 3, opacity: style == .rouge ? 0.6 : 0, color: button.titleLabel?.textColor)
//        button.layer.borderWidth = style == .rouge ? 0 : 1
//        button.layer.borderColor = button.titleLabel?.textColor.cgColor
//    }
//
//}

class BasicView: UIView {

    var tap: UITapGestureRecognizer!
    var dismissKeyboardTargetView: UIView!

    internal func getLabel(text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black, enableMultiline: Bool! = false) -> UILabel {
        let label = UILabel()
        configure(label, text: text, font: font, textColor: textColor)
        if enableMultiline {
            label.numberOfLines = 0
            label.lineBreakMode = .byWordWrapping
        }
        return label
    }

    internal func configure(_ label: UILabel, text: String! = "", font: UIFont! = .systemFont(ofSize: 20), textColor: UIColor! = .black) {
        label.text = text
        label.font = font
        label.textColor = textColor
        addSubview(label)
    }

    internal func hideKeyboardWhenTappedAround() {
        tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false

        let targetView = dismissKeyboardTargetView ?? superview
        targetView?.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        let targetView = dismissKeyboardTargetView ?? superview
        targetView?.removeGestureRecognizer(tap)
        endEditing(true)
    }

}
