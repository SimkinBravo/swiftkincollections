//
//  LeftMenuCollectionViewCell.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 20/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit
import Cartography

class LeftMenuCollectionViewCell: BasicCollectionViewCell {

    static public let reuseId = "LeftMenuCVC"

    let iconImageView = UIImageView()
    let titleLabel = UILabel()
    let separator = UIView()

    static func register(at collectionView: UICollectionView) {
        collectionView.register(LeftMenuCollectionViewCell.self, forCellWithReuseIdentifier: LeftMenuCollectionViewCell.reuseId)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = ""
    }

    private func initElements() {

        iconImageView.image = UIImage(named: "right-arrow")
        addSubview(iconImageView)

        configure(titleLabel, font: .systemFont(ofSize: 16), textColor: .black)

        layer.borderWidth = 2

        separator.backgroundColor = .lightGray
        addSubview(separator)

        let margin = CGPoint(x: 15, y: 15)

        constrain(titleLabel, iconImageView, separator) { title, icon, separator in
            title.left == title.superview!.left + margin.x
            title.right == icon.left - margin.x
            title.height == title.superview!.height - (margin.y * 2)
            icon.right == icon.superview!.right - margin.x
            align(centerY: title.superview!, title, icon)
            separator.width == separator.superview!.width
            separator.height == 1
            separator.bottom == separator.superview!.bottom
        }
    }
}

extension LeftMenuCollectionViewCell: ReusableViewProtocol {

    func willDisplay(with data: ReusableViewDataProtocol) {
        guard let data = data as? MenuCellData else { return }
        titleLabel.text = data.model.text
        backgroundColor = data.model.backgroundColor ?? backgroundColor
        separator.isHidden = data.model.separatorIsHidden
        iconImageView.isHidden = data.model.iconIsHidden
    }
    
}
