//
//  SingleTextCollectionViewCell.swift
//  SwiftkinCollections
//
//  Created by Patricio Bravo Cisneros on 18/01/19.
//  Copyright © 2019 Swiftkin Collection. All rights reserved.
//

import UIKit
import Cartography

class SingleTextCollectionViewCell: BasicCollectionViewCell {

    static public let reuseId = "SingleTitleCVC"

    let titleLabel = UILabel()
    let backView = UIView()

    static func register(at collectionView: UICollectionView) {
        collectionView.register(SingleTextCollectionViewCell.self, forCellWithReuseIdentifier:  SingleTextCollectionViewCell.reuseId)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    private func initElements() {

        addSubview(backView)
        let separator = UIView()

        configure(titleLabel, text: "test text")
        titleLabel.numberOfLines = 2
        titleLabel.lineBreakMode = .byWordWrapping

        addSubview(separator)
//        separator.backgroundColor = .lightGray

        let hEdges: CGFloat = 30
        let vEdges: CGFloat = 10
        let backHEdges = hEdges * 0.5
        constrain(backView, titleLabel, separator) { back, title, line in
            line.height == 1
            back.edges == inset(title.superview!.edges, 0, backHEdges, 0, backHEdges)
            title.edges == inset(title.superview!.edges, vEdges, hEdges, vEdges, hEdges)
            align(left: title, line)
            align(right: title, line)
            line.bottom == title.superview!.bottom
        }
    }

}

extension SingleTextCollectionViewCell: ReusableViewProtocol {

    func willDisplay(with data: ReusableViewDataProtocol) {
        titleLabel.text = data.model.text
        backView.backgroundColor = data.model.backgroundColor ?? .white
    }

}
