//
//  CarouselCollectionViewCell.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 21/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit
import Cartography

class CarouselCollectionViewCell: BasicCollectionViewCell, InteractiveReusableViewProtocol {
    
    static public let reuseId = "CarouselCVC"

    var didEventHandler: ((BasicAction) -> Void)!
    
    var collectionController: CollectionViewController!
    
    static func register(at collectionView: UICollectionView) {
        collectionView.register(CarouselCollectionViewCell.self, forCellWithReuseIdentifier: CarouselCollectionViewCell.reuseId)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initElements()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    private func initElements() {

        collectionController = CollectionViewController()

        collectionController.collectionScrollDirection = .horizontal
        collectionController.configureCollectionView()
//        CarouselVideoCollectionViewCell.register(at: collectionController.collectionView)
        addSubview(collectionController.collectionView)

        constrain(collectionController.collectionView) { collection in
            //product image constrains
            collection.edges == collection.superview!.edges
        }

        guard var iFactory = collectionController.dataSource.cellFactory as? ReusableViewActionsHandlerProtocol else { return }
        iFactory.cellActionsHandlers.append { [weak self] action in
            guard let self = self else { return }
            self.didEventHandler?(BasicAction(id: "click", value: self.collectionController.dataSource.getData(for: action.indexPath) as AnyObject))
        }
    }
}

extension CarouselCollectionViewCell: ReusableViewProtocol {
    
    func willDisplay(with data: ReusableViewDataProtocol) {
        guard let data = data as? CarouselData else { return }

        if collectionController.dataSource.numberOfSections == 0 {
            collectionController.viewDidLoad()
        }
        collectionController.dataSource.setSection(SectionData(items: data.items))
        collectionController.dataSource.cellFactory.gridLayout.columns = 1
        collectionController.collectionView.isPagingEnabled = true
        collectionController.collectionView.reloadData()
        print("CarouselCollectionViewCell:", collectionController.collectionView.frame)
    }

}

struct CarouselData: ReusableViewDataProtocol {

    var data: AnyObject!
    var model: ReusableViewModelProtocol
    var items: [ReusableViewDataProtocol]

    init(items: [ReusableViewDataProtocol]! = [], model: ReusableViewModelProtocol! = ReusableViewModel(reuseId: SingleTextCollectionViewCell.reuseId)) {
        self.items = items
        self.model = model
    }

}
