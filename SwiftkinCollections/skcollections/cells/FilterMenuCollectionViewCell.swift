////
////  FilterMenuCollectionViewCell.swift
////  Pacific
////
////  Created by Patricio Bravo Cisneros on 21/01/19.
////  Copyright © 2019 Lastcode. All rights reserved.
////
//
//import UIKit
//import Cartography
//
//class FilterMenuCollectionViewCell: BasicCollectionViewCell {
//
//    static public let reuseId = "FilterMenuCVC"
//
//    let iconImageView = UIImageView()
//    let titleLabel = UILabel()
//    let separator = UIView()
//
//    static func register(at collectionView: UICollectionView) {
//        collectionView.register(FilterMenuCollectionViewCell.self, forCellWithReuseIdentifier: FilterMenuCollectionViewCell.reuseId)
//    }
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        initElements()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        fatalError("Interface Builder is not supported!")
//    }
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        fatalError("Interface Builder is not supported!")
//    }
//
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        titleLabel.text = ""
//    }
//
//    private func initElements() {
//
//        iconImageView.image = UIImage(named: "filter-icon")
//        addSubview(iconImageView)
//
//        configure(titleLabel, font: .boldSystemFont(ofSize: 14), textColor: .black)
//
//        separator.backgroundColor = .lightGray
//        addSubview(separator)
//
//        let margin = CGPoint(x: 25, y: 15)
//        let textYIndent: CGFloat = 10
//
//        constrain(titleLabel, iconImageView, separator) { title, icon, separator in
//            icon.left == icon.superview!.left + margin.x
//            icon.width == 27
//            icon.height == icon.width
//            title.left == icon.right + textYIndent
//            title.right == title.superview!.right - margin.x
//            title.height == title.superview!.height - (margin.y * 2)
//            align(centerY: title.superview!, title, icon)
//            separator.width == separator.superview!.width
//            separator.height == 1
//            separator.bottom == separator.superview!.bottom
//        }
//    }
//}
//
//extension FilterMenuCollectionViewCell: ReusableViewProtocol {
//
//    func willDisplay(with data: ReusableViewDataProtocol) {
//        guard let data = data as? FilterMenuItem else { return }
//        titleLabel.text = data.name
//    }
//
//}
