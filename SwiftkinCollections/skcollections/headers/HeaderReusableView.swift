//
//  HeaderReusableView.swift
//  ios-challenge
//
//  Created by Patricio Bravo Cisneros on 15/09/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import Cartography

protocol ReusableViewProtocol {

    func willDisplay(with data: ReusableViewDataProtocol)
}

protocol SizableReusableViewProtocol {

    func getSize() -> CGRect
}

protocol InteractiveReusableViewProtocol {

    var didEventHandler: ((BasicAction) -> Void)! { get set }
}

class HeaderReusableView: UICollectionReusableView {

    static public let reuseId = "HeaderRV"

    // Label for headers title
    let label = UILabel()

    static func register(at collectionView: UICollectionView) {
        collectionView.register(HeaderReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderReusableView.reuseId)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        initLabel()
        backgroundColor = .white
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        fatalError("Interface Builder is not supported!")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        label.text = nil
    }

    // Initialize header title
    private func initLabel() {

//        label.font = .navigationBarTitle
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
//        label.textColor = .vermillion
        addSubview(label)

        constrain(label) { view in
            view.edges == inset(view.superview!.edges, 15, 30, 2, 15)
        }
    }
}

extension HeaderReusableView: ReusableViewProtocol {

    func willDisplay(with data: ReusableViewDataProtocol) {
        label.text = data.model.text
    }
}
