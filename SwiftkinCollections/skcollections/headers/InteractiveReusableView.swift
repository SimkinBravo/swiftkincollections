//
//  InteractiveReusableView.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 30/12/18.
//  Copyright © 2018 Lastcode. All rights reserved.
//

import UIKit
import Cartography

class InteractiveReusableView: UICollectionReusableView, InteractiveReusableViewProtocol {

    static public let reuseId = "InteractiveRV"

    let label = UILabel()
    var button: UIButton!

    var didEventHandler: ((BasicAction) -> Void)!

    static func register(at collectionView: UICollectionView) {
        collectionView.register(InteractiveReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: InteractiveReusableView.reuseId)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        backgroundColor = .white
        initLabel()
        initButton()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    // Initialize header title
    private func initLabel() {

//        label.font = .sectionHeaderTitle
        addSubview(label)

        constrain(label) { view in
            view.edges == inset(view.superview!.edges, 15, 15, 2, 115)
        }
    }

    // Initialize header button
    private func initButton() {

        button = UIButton.init(type: .custom)
//        button.setTitle(AppTextId.seeMoreButton.getText(), for: .normal)
//        button.setTitleColor(.scarlet, for: .normal)
//        button.titleLabel?.font = .navigationItemButton
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 5
//        button.layer.borderColor = UIColor.scarlet.cgColor
        addSubview(button)

        constrain(button) { view in
            view.width == 78
            view.height == 25
            view.bottom == view.superview!.bottom - 8
            view.right == view.superview!.right - 14
        }
        button.addTarget(self, action: #selector(didAction), for: .touchUpInside)
    }

    @objc func didAction() {

        didEventHandler?(BasicAction(id: "section"))
    }

}

extension InteractiveReusableView: ReusableViewProtocol {

    func willDisplay(with data: ReusableViewDataProtocol) {

        label.text = data.model.text
//        label.setSectionColor(for: label.text ?? "")
        button.setTitleColor(label.textColor, for: .normal)
        button.layer.borderColor = label.textColor.cgColor

        self.backgroundColor = data.model.backgroundColor ?? .white
    }
}
