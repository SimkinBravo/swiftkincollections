//
//  PAUser.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 07/12/18.
//  Copyright © 2018 Lastcode. All rights reserved.
//

import UIKit
/*
 "idCliente": 4942,
 "razonSocial": "RUIZ GUTIERREZ ALBERTO",
 "rfc": "XAXX010101000",
 "nombre": "ALBERTO",
 "apellidos": "RUIZ GUTIERREZ",
 "token": "VV7Hi09Nv4F3qsHJ3VeH3LQChUmyq_PmtHTtOWk5tL2q5BdtBvF6hpp-79mHEEz4Buc7HgPKA2SyWMmffQk_fnFJypI718APgJyllZBKN0cMrilO21r048q9azE3B4sSGR10gP0ci66XoFtwc2-rv6AsioVXM9TKZaSHNFjZVzJO5hM9IXaSFsCw9xXWBRXrfCTUYVFO72m03eBAImc34aq3CzS9S6GkErjmYN1OcGgRGJ8s-UTQz6LKpnmxJZQpBjlIaYryGmu13dsBvP2oXLNLDwQrUED-2IdA1pHvkefZVSHli0sPyMKrw2A8q20Q",
 "tokenExpires": "2018-12-11T12:10:28.7863964-06:00"
 */
struct PAUser: Decodable {

    let id: Int
    let businessName: String
    let rfc: String
    let name: String
    let lastname: String
    let token: String
}
