//
//  DataSection.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 21/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit

struct SectionData {

    var items: [ReusableViewDataProtocol] = []
    var headerModel: ReusableViewDataProtocol!

    init(items: [ReusableViewDataProtocol]! = [], headerModel: ReusableViewDataProtocol! = nil) {
        self.items = items
        self.headerModel = headerModel
    }

}

extension SectionData {

    init(model: ReusableViewModelProtocol, data: [AnyObject], headerModel: ReusableViewDataProtocol! = nil) {
        for item in data {
            items.append(ReusableViewData(model: model, data: item))
        }
        self.headerModel = headerModel
    }
}
