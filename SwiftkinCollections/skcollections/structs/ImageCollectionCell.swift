////
////  ImageCollectionCell.swift
////  teleSUR
////
////  Created by Patricio Bravo Cisneros on 24/09/18.
////  Copyright © 2018 teleSUR. All rights reserved.
////
//
//import UIKit
//
//class ImageCollectionCell: UICollectionViewCell {
//
//    public weak var imageView: UIImageView!
//    private var labels: [UILabel] = []
//    private var icons: [UIImageView] = []
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        initImageView(frame: frame)
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//
//        fatalError("Interface Builder is not supported!")
//    }
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//        fatalError("Interface Builder is not supported!")
//    }
//
//    override func prepareForReuse() {
//        super.prepareForReuse()
//
//        imageView.image = nil
//        labels.forEach { label in
//            label.text = nil
//        }
//
//    }
//
//    // Initialize custom view
//    private func initImageView(frame: CGRect) {
//
//        // adding product thumbnail
//        let imageView = UIImageView()
//        addSubview(imageView)
//        imageView.frame.size = frame.size
//        imageView.contentMode = .scaleAspectFit
////        imageView.backgroundColor = .lightGray
//        // configuring rounded corners
//        imageView.layer.cornerRadius = 9
//        imageView.layer.masksToBounds = true
//        self.imageView = imageView
//    }
//
//    public func setText(_ text: String, at index: Int, font: UIFont! = nil, textColor: UIColor! = .black) {
//
//        if labels.count < index + 1 {
//            labels.append(getLabel(font: font, textColor: textColor))
//        }
//        labels[index].font = font
//        labels[index].textColor = textColor
//        labels[index].frame.size = frame.size
//        labels[index].text = text
//        labels[index].sizeToFit()
//        labels[index].frame.size.width = frame.size.width - 20
//        labels[index].textAlignment = .center
//    }
//
//    public func setIcons(_ icon: IconModel, at index: Int) {
//
//        if icons.count < index + 1 {
//            icons.append(getIcon(imagename: icon.imagename, tint: icon.tint))
//        }
//        icons[index].center = CGPoint(x: frame.width * icon.position.x, y: frame.height * icon.position.y)
//    }
//
////    public func updateView(_ align: TextStackType, position: TextPosition, maxTextProportion: CGFloat) {
//
//        let spacing: CGFloat = 3
//        var y: CGFloat = 20
//        if position == .outside {
//            let edges: CGFloat = 20
//            let height = frame.height / (1 + maxTextProportion)
//            imageView.frame = CGRect(x: edges, y: edges, width: frame.width - (edges * 2), height: height - (edges * 2))
//            y = imageView.frame.maxY + spacing
//        }
//        switch align {
//        case .top:
//            labels.forEach({ label in
//                label.frame.origin.y = y
//                label.center.x = frame.size.width * 0.5
//                y += label.frame.height + spacing
//            })
//        case .bottom:
//            y = frame.height
//            labels.reversed().forEach({ label in
//                label.frame.origin.y = y - label.frame.height
//                label.center.x = frame.midX
//                y = label.frame.origin.y + spacing
//            })
//        }
//        labels.first?.numberOfLines = 2
//    }
//
//    private func getLabel(font: UIFont! = .systemFont(ofSize: 12), textColor: UIColor! = .white) -> UILabel {
//        let label = UILabel()
//        label.frame.size = CGSize(width: frame.size.width, height: frame.size.height)
//        label.numberOfLines = 0
//        label.lineBreakMode = .byWordWrapping
//        label.textColor = textColor
//        label.font = font
//        //        label.labelDropShadow()
//        addSubview(label)
//        return label
//    }
//
//    private func getIcon(imagename: String, tint: UIColor! = nil) -> UIImageView {
//        var image = UIImage(named: imagename)
//        if tint != nil {
//            image = image?.maskWith(color: tint)
//        }
//        let imageView = UIImageView(image: image)
//        addSubview(imageView)
//        return imageView
//    }
//
//}
