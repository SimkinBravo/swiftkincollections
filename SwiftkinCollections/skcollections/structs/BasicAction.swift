//
//  BasicAction.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 29/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit

struct BasicAction {

    let id: String
    let value: AnyObject!

    init(id: String, value: AnyObject! = nil) {
        self.id = id
        self.value = value
    }

}
