//
//  CollectionGrid.swift
//  teleSUR
//
//  Created by Patricio Bravo Cisneros on 21/10/18.
//  Copyright © 2018 teleSUR. All rights reserved.
//

import UIKit

protocol GridLayoutProtocol {

    var insets: UIEdgeInsets { get set }
    var columns: Int { get set }
    var heightProportion: CGFloat { get set }
    var headerHeight: CGFloat! { get set }

    func getColumns(for collectionView: UICollectionView, at indexPath: IndexPath, config: ReusableViewModelProtocol) -> CGFloat
    func getItemSize(for collectionView: UICollectionView, at indexPath: IndexPath, config: ReusableViewModelProtocol) -> CGSize
    func getHeaderSize(for collectionView: UICollectionView, section: Int, config: ReusableViewModelProtocol) -> CGSize
    func getMinimumLineSpacingForSectionAt() -> CGFloat
    func getMinimumInteritemSpacingForSectionAt() -> CGFloat
    func getInsets() -> UIEdgeInsets
}

class GridLayout: GridLayoutProtocol {

    var columns: Int
    var insets: UIEdgeInsets
    var heightProportion: CGFloat
    var columnsExceptionsAt: [Int: Int]! = [:]
    var headerHeight: CGFloat!

    init(columns: Int! = 1, insets: UIEdgeInsets! = UIEdgeInsets(), heightProportion: CGFloat! = 1.27, headerHeight: CGFloat! = 48) {
        self.columns = columns
        self.insets = insets
        self.heightProportion = heightProportion
        self.headerHeight = headerHeight
    }

    public func getColumns(for collectionView: UICollectionView, at indexPath: IndexPath, config: ReusableViewModelProtocol) -> CGFloat {
        guard let columns = config.columns else {
            return CGFloat(self.columns)
        }
        return columns
    }

    public func getItemSize(for collectionView: UICollectionView, at indexPath: IndexPath, config: ReusableViewModelProtocol) -> CGSize {
        var width = collectionView.bounds.size.width
        let columns = getColumns(for: collectionView, at: indexPath, config: config)
        width = (width - (insets.left * (columns + 1))) / columns
        let finalHPropotion = config.heightProportion ?? heightProportion
        return CGSize(width: width, height: width * finalHPropotion)
    }

    public func getHeaderSize(for collectionView: UICollectionView, section: Int, config: ReusableViewModelProtocol) -> CGSize {

        return config.size ?? CGSize(width: collectionView.frame.width, height: headerHeight)
    }

    public func getMinimumLineSpacingForSectionAt() -> CGFloat {

        return insets.left
    }

    public func getMinimumInteritemSpacingForSectionAt() -> CGFloat {

        return insets.left
    }

    public func getInsets() -> UIEdgeInsets {

        return insets
    }

}
