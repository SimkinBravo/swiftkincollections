//
//  ImageCollectionCellModel.swift
//  teleSUR
//
//  Created by Patricio Bravo Cisneros on 24/09/18.
//  Copyright © 2018 teleSUR. All rights reserved.
//

import UIKit

//enum AspectRatio {
//
//    case ratio3_4
//    case square
//    case ratio4_3
//    case ratio3_2
//    case ratio16_9
//}
//
//enum TextStackType {
//
//    case top
//    case bottom
//}
//
//enum TextPosition {
//
//    case inside
//    case outside
//}

//protocol ImageCellModelProtocol: CellModelProtocol {
//
//    var aspect: AspectRatio! { get set }
//    var textPosition: TextPosition! { get set }
//    var maxTextProportion: CGFloat! { get set }
//}

//struct IconModel {
//
//    var imagename: String
//    var position: CGPoint
//    var tint: UIColor!
//
//    init(imagename: String, position: CGPoint! = .zero, tint: UIColor! = nil) {
//        self.imagename = imagename
//        self.position = position
//        self.tint = tint
//    }
//
//}

//struct ImageCollectionCellModel: ImageCellModelProtocol {
//
//    var borderIsHidden: Bool! = false
//    static var reuseId = String(describing: ImageCollectionCell.self)
//    var textPosition: TextPosition! = .outside
//    var maxTextProportion: CGFloat! = 0.5
//
//    var aspect: AspectRatio! = .square
//
//    var icons: [IconModel]!
//    var texts: [LabelModel]! = [LabelModel(font: UIFont.boldSystemFont(ofSize: 14), textColor: .darkGray),
//                                LabelModel(font: UIFont.systemFont(ofSize: 14), textColor: .blueBrand),
//                                LabelModel(font: UIFont.systemFont(ofSize: 16), textColor: .darkGray)
////                                LabelModel(font: UIFont.systemFont(ofSize: 10), textColor: .white, backgroundColor: .yellowPromo, position: CGPoint(x: 15, y: 15)),
////                                LabelModel(font: UIFont.systemFont(ofSize: 10), textColor: .white, backgroundColor: .orangeSeason, position: CGPoint(x: 100, y: 15))
//                                ]
//    var thumbs: [URL]!
//
//    var background: UIColor!
//    var cornerRadius: Int!
//
//    init() {
//
//    }
//}
//
//extension ImageCollectionCellModel {
//
//    init(product: PAProduct) {
//
//        let url = URL(string: product.thumb.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
//        thumbs = [url] as? [URL]
//        texts[0].text = product.desc
//        texts[1].text = product.brand
////        print(data.price)
//        if product.price != 0 {
//            texts[2].text = "$\(product.price)"
//        }
////        if data.promo {
////            texts[3].text = "PROMOCIÓN"
////        }
////        if data.season {
////            texts[4].text = "TEMPORADA"
////        }
//        icons = [IconModel(imagename: "cart-icon", position: CGPoint(x: 0.1, y: 0.9), tint: .lightGray),
//                 IconModel(imagename: "fav-icon", position: CGPoint(x: 0.9, y: 0.9), tint: .lightGray)]
//    }
//
//    init(brand: PABrand) {
//        thumbs = []
//        texts = [LabelModel(text: brand.name, font: UIFont.boldSystemFont(ofSize: 14), textColor: .blueBrand)]
//        icons = [IconModel(imagename: "placeholder", position: CGPoint(x: 0.5, y: 0.3))]
//    }
//
//    init(maker: PAMaker) {
//        let pluralStr = maker.productsCount != 0 ? "s" : ""
//        texts = [LabelModel(text: maker.name, font: UIFont.boldSystemFont(ofSize: 14), textColor: .blueBrand, position: CGPoint(x: 0.5, y: 0.25)),
//                 LabelModel(text: "\(maker.productsCount) producto\(pluralStr)", font: UIFont.boldSystemFont(ofSize: 12), textColor: .black)]
//        icons = []
//    }
//
//    init(address: PAAddress) {
//        texts = [LabelModel(text: "\(address.street) \(address.number) \(address.internalNumber)", font: UIFont.boldSystemFont(ofSize: 18), textColor: .blueBrand),
//                 LabelModel(text: "\(address.district) \(address.municipality)", font: UIFont.boldSystemFont(ofSize: 14), textColor: .darkGray),
//                 LabelModel(text: "\(address.country) \(address.postalCode)", font: UIFont.boldSystemFont(ofSize: 14), textColor: .darkGray)]
//        icons = []
//    }
//
//}
