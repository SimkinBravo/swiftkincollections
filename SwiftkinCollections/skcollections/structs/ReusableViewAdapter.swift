//
//  ReusableViewData.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 18/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit

protocol ReusableViewDataProtocol {

    var data: AnyObject! { get set }
    var model: ReusableViewModelProtocol { get set }
}

struct ReusableViewData: ReusableViewDataProtocol {

    var data: AnyObject!
    var model: ReusableViewModelProtocol

    init(model: ReusableViewModelProtocol, data: AnyObject! = nil) {
        self.model = model
        self.data = data
    }
}

protocol ReusableViewModelProtocol {
    //For setting reuseId for adapting an api data struct to a Collection or Table ViewCell
    var reuseId: String! { set get }
    
    var size: CGSize! { set get }
    //For setting specific widht/height proportion for specific cell
    var heightProportion: CGFloat! { set get }
    //For setting specific num of Columns for specific row
    var columns: CGFloat! { set get }
    //For setting a title for specific cell //Temporal
    var text: String! { set get }
    //For setting a imagename for specific cell //Temporal
    var imagename: String! { set get }
    //For setting background color for specific cell //Temporal
    var backgroundColor: UIColor! { set get }
    //For setting if arrow icon is hidden for cell //Temporal
    var iconIsHidden: Bool! { set get }
    //For setting if separator is hidden for cell //Temporal
    var separatorIsHidden: Bool! { set get }
    //For setting if controls for userInteractivity is hidden for cell //Temporal
    var userInterfaceIsEnabled: Bool! { set get }
}

struct ReusableViewModel: ReusableViewModelProtocol {
    //vars to conform ReusableViewIdProtocol
    var text: String!
    var imagename: String!
    var size: CGSize!
    var heightProportion: CGFloat!
    var columns: CGFloat!
    var reuseId: String!
    var backgroundColor: UIColor!
    var iconIsHidden: Bool!
    var separatorIsHidden: Bool!
    var userInterfaceIsEnabled: Bool!

    init(reuseId: String! = nil, text: String! = nil, imagename: String! = nil, heightProportion: CGFloat! = nil, backgroundColor: UIColor! = nil, separatorIsHidden: Bool! = nil, columns: CGFloat! = nil, userInterfaceIsEnabled: Bool! = true, size: CGSize! = nil, iconIsHidden: Bool! = nil) {
        self.reuseId = reuseId
        self.text = text
        self.imagename = imagename
        self.size = size
        self.heightProportion = heightProportion
        self.backgroundColor = backgroundColor
        self.separatorIsHidden = separatorIsHidden
        self.userInterfaceIsEnabled = userInterfaceIsEnabled
        self.iconIsHidden = iconIsHidden
        self.columns = columns
    }

}
