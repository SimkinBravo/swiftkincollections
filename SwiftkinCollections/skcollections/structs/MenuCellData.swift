//
//  MenuCellData.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 30/12/18.
//  Copyright © 2018 Lastcode. All rights reserved.
//

import UIKit

struct MenuCellData: ReusableViewDataProtocol {
    let id: String!
    var model: ReusableViewModelProtocol = ReusableViewModel(reuseId: LeftMenuCollectionViewCell.reuseId)
    var data: AnyObject!
}

extension MenuCellData {

    init(id: String! = "", title: String! = "", backgroundColor: UIColor! = .white, iconIsHidden: Bool! = false, separatorIsHidden: Bool! = false) {
        self.id = id
        model.text = title
        model.backgroundColor = backgroundColor
        model.iconIsHidden = iconIsHidden
        model.separatorIsHidden = separatorIsHidden
    }

}
