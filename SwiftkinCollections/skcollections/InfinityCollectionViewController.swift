//
//  InfinityCollectionViewController.swift
//  Pacific
//
//  Created by Patricio Bravo Cisneros on 22/01/19.
//  Copyright © 2019 Lastcode. All rights reserved.
//

import UIKit

class InfinityCollectionViewController: CollectionViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func configureDataSource() {
        super.configureDataSource()
        dataSource.defaultSectionData = SectionData(headerModel: ReusableViewData(model: ReusableViewModel(reuseId: InteractiveReusableView.reuseId, text: "model.title")))
    }

    override func getDataSource() -> DataSourceProtocol {
        return InfinityDataSource()
    }

}
