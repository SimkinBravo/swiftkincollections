//
//  FavoritesViewController.swift
//  ios-challenge
//
//  Created by Patricio Bravo Cisneros on 14/09/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import Cartography

class CollectionViewController: UIViewController {

    var dataSource: DataSourceProtocol!
    var collectionView: UICollectionView!

    override func loadView() {
        super.loadView()

//        initCollectionView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureDataSource()
        configureCollectionView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if dataSource?.numberOfSections == 0 {
            loadData()
        }
    }

    // Configuring CollectionView
    public func configureCollectionView() {
        
        if collectionView == nil {
            initCollectionView()
        }
        collectionView.dataSource = dataSource
        collectionView.delegate = self
    }

    public func setDataSource(_ dataSource: DataSourceProtocol) {

        self.dataSource = dataSource
        setupEventHandlers()
    }

    internal func configureDataSource() {

        if dataSource == nil {
            setDataSource(getDataSource())
        }
        dataSource.onDataChange = { [weak self] action in
            guard let self = self else { return }
            self.onDataSourceChangeHandler(action: action)
        }
    }

    internal func onDataSourceChangeHandler(action: BasicAction) {
        switch action.id {
        case "clear", "section":
            self.collectionView.reloadData()
        case "update-item":
            self.collectionView.reloadItems(at: [action.value as! IndexPath])
        case "next-page":
            self.loadData()
        default:
            print("no action")
        }
    }

    internal func getDataSource() -> DataSourceProtocol {
        return DataSource()
    }

    // For custom collections to override
    internal func loadData() {}

    var collectionScrollDirection: UICollectionView.ScrollDirection = .vertical
    // Initialize CollectionView
    internal func initCollectionView() {

        let layout = UICollectionViewFlowLayout()
        layout.sectionHeadersPinToVisibleBounds = collectionScrollDirection == .vertical
        layout.scrollDirection = collectionScrollDirection
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        view.addSubview(collectionView)
        constrain(collectionView) { collection in
            collection.edges == collection.superview!.edges
        }
    }

    internal func cellEventHandler(_ action: CellAction) {   }

    internal func headerEventHandler(_ action: CellAction) {   }

    private func setupEventHandlers() {
        guard var cellFactory = dataSource.cellFactory as? ReusableViewActionsHandlerProtocol else { return }
        cellFactory.cellActionsHandlers.append { [weak self] action in
            guard let self = self else { return }
            self.cellEventHandler(action)
        }
        cellFactory.headerActionsHandlers.append { [weak self] action in
            guard let self = self else { return }
            self.headerEventHandler(action)
        }
    }

//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        super.scrollViewDidScroll(scrollView)
//    }

}

extension CollectionViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let configurableCell = cell as? ReusableViewProtocol else { return }
        configurableCell.willDisplay(with: dataSource.getData(for: indexPath))
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cellFactory = dataSource.cellFactory as? ReusableViewActionsHandlerProtocol else { return }
        cellFactory.sendCellAction(CellAction(indexPath: indexPath, id: "click"))
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let data = dataSource.getData(for: indexPath)
        return dataSource.cellFactory.gridLayout.getItemSize(for: collectionView, at: indexPath, config: data.model)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if dataSource.getSection(at: section).headerModel == nil {
            return CGSize()
        }
        let data = dataSource.getSection(at: section)
        return dataSource.cellFactory.gridLayout.getHeaderSize(for: collectionView, section: section, config: data?.headerModel.model ?? ReusableViewModel())
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return dataSource.cellFactory.gridLayout.getMinimumLineSpacingForSectionAt()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return dataSource.cellFactory.gridLayout.getMinimumInteritemSpacingForSectionAt()
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return dataSource.cellFactory.gridLayout.getInsets()
    }

}
