Pod::Spec.new do |s|

# 1
  s.platform = :ios
  s.ios.deployment_target = '12.0'

  s.name         = "SwiftkinCollections"
  s.summary      = "SwiftkinCollections trying to make collections easy and interactive."
  s.requires_arc = true

# 2
  s.version      = "0.1.10"

# 3
  s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
  s.author = { "Simkin Bravo" => "patricio.bravoc@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
  s.homepage = "https://gitlab.com/SimkinBravo/"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
  s.source = { :git => "https://gitlab.com/SimkinBravo/swiftkincollections.git",
  :tag => "#{s.version}" }

# 7
  s.framework = "UIKit"
  s.dependency 'Cartography', '~> 3.0'

# 8
  s.source_files = "SwiftkinCollections/**/*.{swift}"

# 9
  # s.resources = "SwiftkinCollections/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"

# 10
  s.swift_version = "4.2"

end
